import json
import os
from os.path import isfile
from pathlib import Path
from typing import Dict


def test_checkers(hub, idem_cli):
    plugin_conf_file = os.environ["PLUGIN_CONFIG_FILE"] or None
    if not isfile(plugin_conf_file):
        raise Exception(
            f"The provided plugin conf file: {plugin_conf_file} does not exist."
        )

    test_config = {}
    with open(plugin_conf_file) as config_file:
        test_config = json.load(config_file)

    base_rev = test_config["base_version"]
    target_rev = test_config["target_version"]

    base_schema_json = hub.tool.schema.scm_utils.fetch_schema(test_config, base_rev)
    base_schema = json.loads(base_schema_json)

    current_schema_json = hub.tool.schema.scm_utils.fetch_schema(
        test_config, target_rev
    )
    current_schema = json.loads(current_schema_json)

    collector = hub.tool.schema.schema_compatibility_checker.check(
        current_schema,
        base_schema,
        get_bool_attr(test_config, "validate_all_resources", "True"),
        test_config["resources_to_validate"],
    )

    compatibility_report = collector.revisions(target_rev, base_rev).report_as_json()
    plugin_conf_file_name = Path(plugin_conf_file).stem
    with open(f"compatibility-report-{plugin_conf_file_name}.json", "w") as f:
        f.write(compatibility_report)

    print(f"\nSchema compatibility check completed ({base_rev} -> {target_rev})")

    if collector.has_breaking_changes and get_bool_attr(
        test_config, "fail_on_breaking_changes", "False"
    ):
        raise Exception(f"\nSchema compatibility check contains breaking changes.")


def get_bool_attr(test_schema: Dict, attribute: str, default_value: str) -> bool:
    attr_value = default_value
    if attribute in test_schema and test_schema[attribute]:
        attr_value = test_schema[attribute]
    return eval(attr_value)
